import aioschedule as schedule
from threading import Thread
import asyncio
import time
import requests
import aiohttp
import json
import copy
import pycurl
import certifi
from io import StringIO
from io import BytesIO

########################################
##   BOT FOR ASYNCHRONOUS TEST URLS   ##
########################################

gUrlsQueue = asyncio.Queue()

# general header requests which we check 
gheaders = [
    'Content-Type: application/json',
    'Accept: application/json'
]

# put here api to save information about the request
gUrlResponseStore = "https://"

# put here api to get urls to test
gUrlToGetUrls = 'http://'

# put here api to get config for bot
gUrlToGetConfig = 'http://'

gBotConfig = None
gSendedRequest = 0
gRequestListInAwait = {}


class PyCurlResponse:
    ''' Response from sended request '''
    def __init__(self, url = '', method = 'GET', data = None, headers= None, http_code = 789, total_seconds = 0, dictionary = {}):
        self.url = url
        self.method = method
        self.data = data
        self.headers = headers
        self.http_code = http_code
        self.total_seconds = total_seconds
        self.dictionary = dictionary
    def __str__(self):
        return self.method + ' | ' + self.url + ' | ' + str(self.http_code)


class PyCurlEngine:
    ''' Engine to send requesta '''
    @staticmethod
    def engine(url, headers = None, data = None, method = "GET", cookies = ''):
        curl = pycurl.Curl()

        buff = BytesIO()
        hdr = BytesIO()

        curl.setopt(pycurl.URL, url)

        if headers:
            curl.setopt(pycurl.HTTPHEADER, headers)

        curl.setopt(pycurl.CAINFO, certifi.where())

        curl.setopt(pycurl.WRITEFUNCTION, hdr.write)
        curl.setopt(pycurl.HEADERFUNCTION, buff.write)

        if data and method=="POST":
            dataJson = json.dumps(data)
            curl.setopt(pycurl.POST, True)
            curl.setopt(pycurl.POSTFIELDS, dataJson)
       
        curl.setopt(pycurl.COOKIE, cookies)

        try:
            curl.perform()
            http_code = curl.getinfo(pycurl.RESPONSE_CODE)
            if(http_code == 0):
                http_code += 900
        except pycurl.error as e:
            http_code = e.args[0] + 900

        total_seconds = curl.getinfo(pycurl.TOTAL_TIME)

        try:
            dictionary = json.loads(hdr.getvalue())
        except Exception as e:
            dictionary = None

        curl.close()
        
        return PyCurlResponse(url= url, method= method, data= data, headers= headers, http_code= http_code, total_seconds= total_seconds, dictionary= dictionary)



class Url:
    ''' Url '''
    def __init__(self, id, group_id ,url, method, headers, params, cookies, frequency):
        self.id = id
        self.group_id = group_id
        self.url = url
        self.method = method
        self.headers = self.__convertHeader(headers)
        self.params = params
        self.cookies = self.__convertCookies(cookies) 
        self.frequency = frequency
    def __str__(self):
        return '\nUrl: ' + self.url + ' | Method: ' + self.method + ' | Frequency: ' + str(self.frequency) + ' | id: ' + str(self.id) + ' | group_id:' + str(self.group_id) + '\n'
    def __convertHeader(self,headersObj):
        headersList = gheaders
        if(headersObj == None):
            return headersList
        for key in headersObj:
            headersList.append(str(key)+": "+str(headersObj[key]))
        return headersList
    def __convertCookies(self,cookiesObj):
        cookiesStr = ''
        if(cookiesObj == None):
            return cookiesStr
        for key in cookiesObj:
            cookiesStr += str(key)+" = "+str(cookiesObj[key]) + '; '
        return cookiesStr
    def sendRequestToCheckResponse(self):
        print(f"Task getting URL: {self.url}")
        response = PyCurlEngine.engine(url=self.url, method=self.method, headers=self.headers, data=self.params, cookies=self.cookies)
        print("Task elapsed time: " + str(response.total_seconds) + " | Status: " + str(response.http_code) + f" | URL: {response.url}")
        self.__sendRequestToSaveResponse(response)
    def __sendRequestToSaveResponse(self, response):
        dataJson = {'group_id':self.group_id,'url_id':self.id,'timestamp':int(time.time()),'http_response':response.http_code,'time_response':response.total_seconds}
        responseSaved = PyCurlEngine.engine(url=gUrlResponseStore, method="POST", data=dataJson, headers=gheaders)
       
        



class Urls:
    ''' Urls '''
    __url_to_get_urls = gUrlToGetUrls
    urlsJson = {}
    urlsObjs = []
    def __init__(self):
        self.downloadUrls()
    def __str__(self):
        return '\nUrls:\n\n' + str(self.urlsJson)
    def downloadUrls(self):
        response = PyCurlEngine.engine(url=self.__url_to_get_urls,headers=gheaders)
        self.urlsJson = response.dictionary['models']['hits']['hits']
        self.__convertUrlsJsonToUrlsObjs()
    def __valueIfNone(self, array, key):
        if(key in array and array[key]):
            return array[key]
        return {}
    def __convertUrlsJsonToUrlsObjs(self):
        urlsObjs = []
        for item in self.urlsJson:
            source = item['_source']
            if(source['group_id'] is None):
                group_id = None
            else:
                group_id = source['group_id']
            urlsObjs.append(Url(id = item['_id'],group_id = group_id,url = source['url'],method = source['method'],headers = self.__valueIfNone(source,'headers'),params = self.__valueIfNone(source,'params'),cookies = self.__valueIfNone(source,'cookies'),frequency = int(source['frequency'])))
        self.urlsObjs = urlsObjs


class BotConfig:
    ''' Bot Config '''
    __url_to_get_config = gUrlToGetConfig
    __time_in_seconded_to_sending_request = 1
    __max_number_requests_on_time = 1
    def __init__(self):
        self.updateConfig()
    def __downloadConfig(self):
        response = PyCurlEngine.engine(url=self.__url_to_get_config, headers=gheaders)
        return response.dictionary['model']['_source']
    @property
    def time_in_seconded_to_sending_request(self):
        return self.__time_in_seconded_to_sending_request
    @property
    def max_number_requests_on_time(self):
        return self.__max_number_requests_on_time
    def updateConfig(self):
        data = self.__downloadConfig()
        self.__time_in_seconded_to_sending_request = int(data['time_to_requests'])
        self.__max_number_requests_on_time = int(data['max_requests_on_time'])



async def addToQueue(urlObj):
    keyTime = int(time.time())
    if(keyTime not in gRequestListInAwait):
        gRequestListInAwait[keyTime] = {}
    gRequestListInAwait[keyTime][urlObj.id] = urlObj

async def resetSendedRequest():
    global gSendedRequest
    gSendedRequest = 0

async def sendRequestFromQueue(loop):
    urlsObjToSend = []
    global gSendedRequest
    global gBotConfig
    for keyTime in gRequestListInAwait:
        urls = copy.deepcopy(gRequestListInAwait[keyTime])
        for keyUrl,valueUrl in urls.items():
            if(gSendedRequest < gBotConfig.max_number_requests_on_time):
                gSendedRequest += 1
                del gRequestListInAwait[keyTime][keyUrl]               
                urlsObjToSend.append(valueUrl)
        if(gRequestListInAwait[keyTime] == {}):
            del gRequestListInAwait[keyTime]
        break

    if(urlsObjToSend):
        for task in urlsObjToSend:
            t = Thread(target=task.sendRequestToCheckResponse)
            t.start()






if __name__ == "__main__":
    gBotConfig = BotConfig()

    for urlObj in Urls().urlsObjs:
        schedule.every(urlObj.frequency).seconds.do(addToQueue, urlObj=urlObj)

    loop = asyncio.get_event_loop()

    schedule.every(1).seconds.do(sendRequestFromQueue, loop = loop)
    schedule.every(gBotConfig.time_in_seconded_to_sending_request).seconds.do(resetSendedRequest)


    while True:
        loop.run_until_complete(schedule.run_pending())
        time.sleep(0.1)